# Hi, I'm Thanachai! 👋
Welcome to my repository! This project serves as a portfolio to showcase my knowledge and skills in [technology/field], with a focus on various concepts and best practices. I am passionate about Infrastructure as Code (Terraform and Ansible), Kubernetes cluster setup using Kubespray (On-premise), and developing Kubernetes middleware applications.

## 🚀 About Me
Hello, I'm Thanachai, a dedicated Cloud Engineer with extensive experience in managing both on-premise (VMWare-based) and Public Cloud (Azure) environments. My passion lies in crafting efficient and scalable infrastructure solutions that embrace the principles of DevOps and Site Reliability Engineering (SRE) culture.

## Resource
### Infrastructure as a Code
Explore my Infrastructure as Code (IaC) projects that demonstrate the automation and management of cloud resources using Terraform and Ansible. The GitLab repository for IaC can be found [here](https://gitlab.com/thanachai.tre/iac).
### Kubespray
In this section, I present my Kubernetes middleware applications, showcasing my ability to develop and deploy applications on Kubernetes. The GitLab repository for Kubernetes applications can be found [here](https://gitlab.com/thanachai.tre/kubespray).
### Kubernetes Application
In this section, I present my Kubernetes middleware applications, showcasing my ability to develop and deploy applications on Kubernetes. The GitLab repository for Kubernetes applications can be found [here](https://gitlab.com/thanachai.tre/k8s-application).

## Programming
I try to learning about programming language JavaScript, Node.JS, React, Next.JS and etc. You can find my experimentation project. [here](https://gitlab.com/programming1121434)

## Contact Me
If you have any questions, suggestions, or collaboration opportunities, feel free to reach out to me via email at thanachai.tre@gmail.com connect with me on LinkedIn: [LinkedIn Profile](https://www.linkedin.com/in/thanachai-treratdilokkul-bb2622208/)
## License
This project is licensed under the MIT License - see the [LICENSE](./LICENSE) file for details.

## Certificate
![AZ-900](./src/AZ900-Thanachai.png)
![AZ-104](./src/AZ104-Thanachai.png)
![1Z0-1085-23](./src/OCI_Foundation-Thanachai.png)